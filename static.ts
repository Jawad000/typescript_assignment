import { constants } from "buffer";

class Circle{
    static pi:number =3.14;

    static calculateArea(radius:number)
    {
        return this.pi*radius*radius;
    }
}

console.log(Circle.pi);
console.log(Circle.calculateArea(5));


class MYCircle {
    static pi = 3.14;
    pi = 3;
}

MYCircle.pi; // returns 3.14

let circleObj = new MYCircle();
circleObj.pi; // returns 3



class THeCircle {
    static pi = 3.14;

    static calculateArea(radius:number) {
        return this.pi * radius * radius;
    }

    calculateCircumference(radius:number):number { 
        return 2 * Circle.pi * radius;
    }
}

Circle.calculateArea(5); // returns 78.5

let circleObjnew = new THeCircle();
circleObjnew.calculateCircumference(5) // returns 31.4000000
//circleObj.calculateArea(); <-- cannot call this










