let something: any = "Hello World!"; 
something = 23;
something = true;
/*2*/
let arr: any[] = ["John", 212, true]; 
arr.push("Smith"); 
console.log(arr); //Output: [ 'John', 212, true, 'Smith' ] 