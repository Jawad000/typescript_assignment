type Product = { name: string, price: number };
let p: Product = {price: 100, name: 'Monitor'};
console.log(p);
//function
type StringRemover = (input: string, index: number) => string;
let remover: StringRemover = function (str: string, i: number): string {
    return str.substring(i);
}
let s = remover("Hi there", 3);
console.log(s);
//array
type State = [string, boolean];
let a: State = ["active", true];
console.log(a);





/*2*/

type num = number | string;
function square(n: num): number {
    if (typeof n === 'string') {
        n = parseInt(n);
    }
    return Math.pow(n, 2);
}
console.log(square(3));
console.log(square("5"));


/*3*/

type chars = string;
function show(param: chars): void {
    console.log(param);
}
show("hello");