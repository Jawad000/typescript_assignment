/// <reference path="C:\Users\xawad\Desktop\Typescript Practise\namespace\StringUtility.ts" />



export class Employee {
    empCode: number;
    empName: string;
    constructor(name: string, code: number) {
        this.empName = StringUtility.ToCapital(name);
        this.empCode = code;
    }
    displayEmployee() {
        console.log ("Employee Code: " + this.empCode + ", Employee Name: " + this.empName );
    }
}

let empObj = new Employee("jawad",2424);
console.log(empObj.displayEmployee());