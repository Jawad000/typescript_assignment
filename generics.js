"use strict";
/*1*/
var KeyValuePair = /** @class */ (function () {
    function KeyValuePair() {
    }
    KeyValuePair.prototype.setKeyValue = function (key, val) {
        this.key = key;
        this.val = val;
    };
    KeyValuePair.prototype.display = function () {
        console.log("Key = " + this.key + ", val = " + this.val);
    };
    return KeyValuePair;
}());
var kvp1 = new KeyValuePair();
kvp1.setKeyValue(1, "Steve");
kvp1.display();
var kvp2 = new KeyValuePair();
kvp2.setKeyValue("Kalat", "Jawad");
kvp2.display();
;
var kvProcessor = /** @class */ (function () {
    function kvProcessor() {
    }
    kvProcessor.prototype.process = function (key, val) {
        console.log("Key = " + key + ", val = " + val);
    };
    return kvProcessor;
}());
var proc = new kvProcessor();
proc.process(1, 'Bill');
