"use strict";
var PrintMedia;
(function (PrintMedia) {
    PrintMedia[PrintMedia["Newspaper"] = 1] = "Newspaper";
    PrintMedia[PrintMedia["NewLetter"] = 2] = "NewLetter";
    PrintMedia[PrintMedia["Magazine"] = 3] = "Magazine";
    PrintMedia[PrintMedia["Book"] = 4] = "Book";
})(PrintMedia || (PrintMedia = {}));
function getMedia(mediaName) {
    if (mediaName == 'Forbes' || mediaName == 'Outlook') {
        return PrintMedia.Magazine;
    }
}
var mediaType = getMedia('Forbes');
console.log(mediaType);
var hey;
(function (hey) {
    hey["Newspaper"] = "NEWSPAPER";
    hey["Newsletter"] = "NEWSLETTER";
    hey["Magazine"] = "MAGAZINE";
    hey["Book"] = "BOOK";
})(hey || (hey = {}));
// Access String Enum 
hey.Newspaper; //returns NEWSPAPER
hey['Magazine']; //returns MAGAZINE
var reverMapping;
(function (reverMapping) {
    reverMapping[reverMapping["Newspaper"] = 1] = "Newspaper";
    reverMapping[reverMapping["Newsletter"] = 2] = "Newsletter";
    reverMapping[reverMapping["Magazine"] = 3] = "Magazine";
    reverMapping[reverMapping["Book"] = 4] = "Book";
})(reverMapping || (reverMapping = {}));
console.log(PrintMedia);
