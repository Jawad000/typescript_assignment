"use strict";
function square(n) {
    if (typeof n === 'string') {
        n = parseInt(n);
    }
    return Math.pow(n, 2);
}
console.log(square(3));
console.log(square("5"));
function doAlign(alignment) {
    console.log(alignment);
}
doAlign("Left");
function setWidth(w) {
    console.log(w);
}
setWidth(100);
