"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var JobHolders = /** @class */ (function () {
    function JobHolders(name, salary) {
        this.jobholdername = name;
        this.jobholdersalary = salary;
    }
    return JobHolders;
}());
var SalesEmployee = /** @class */ (function (_super) {
    __extends(SalesEmployee, _super);
    function SalesEmployee(jobholdername, jobholdersalary, department) {
        var _this = _super.call(this, jobholdername, jobholdersalary) || this;
        _this.department = department;
        return _this;
    }
    SalesEmployee.prototype.find = function (arg0) {
        throw new Error("Method not implemented.");
    };
    SalesEmployee.prototype.getSalaryForYear = function () {
        return this.jobholdersalary * 12;
    };
    return SalesEmployee;
}(JobHolders));
var emp = new SalesEmployee("jawad", 10, "sales");
console.log(emp.department);
console.log(emp.getSalaryForYear());
//console.log(emp.jobholdersalary);
var PersonNew = /** @class */ (function () {
    function PersonNew(name) {
        this.name = name;
    }
    return PersonNew;
}());
var EmployeeNewnew = /** @class */ (function (_super) {
    __extends(EmployeeNewnew, _super);
    function EmployeeNewnew(name, department) {
        var _this = _super.call(this, name) || this;
        _this.department = department;
        return _this;
    }
    EmployeeNewnew.prototype.getElevatorPitch = function () {
        return "Hello, my name is " + this.name + " and I work in " + this.department + ".";
    };
    return EmployeeNewnew;
}(Person));
var howard1 = new EmployeeNewnew("Howard", "Sales");
console.log(howard.getElevatorPitch());
console.log(howard.name); // error
var MyClass = /** @class */ (function () {
    function MyClass(theName) {
        this.name = theName;
    }
    return MyClass;
}());
var MyPeoples = /** @class */ (function (_super) {
    __extends(MyPeoples, _super);
    function MyPeoples(name, department) {
        var _this = _super.call(this, name) || this;
        _this.department = department;
        return _this;
    }
    MyPeoples.prototype.getDetails = function () {
        return "Hello, my name is " + this.name + " and I work in " + this.department + ".";
    };
    return MyPeoples;
}(MyClass));
var jawad = new MyPeoples("Jawad", "Sales");
console.log(jawad.getDetails());
///let john = new MyClass("John"); // Error: The 'Person' constructor is protected
