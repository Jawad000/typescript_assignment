"use strict";
var MyClass = /** @class */ (function () {
    function MyClass(Myroll, Myname, Myaddress, OwnNationality) {
        this.roll = Myroll,
            this.name = Myname,
            this.address = Myaddress,
            this.Nationality = OwnNationality;
    }
    MyClass.prototype.getSalary = function () {
        return 12000 * this.roll;
    };
    MyClass.prototype.totalCostForMonth = function (x) {
        return x - 1000;
    };
    return MyClass;
}());
var ClasssObj = new MyClass(12, 'Jawad', 'Dhaka', 'Bangladeshi');
var count = (ClasssObj.getSalary());
console.log(count);
console.log(ClasssObj.address);
console.log(ClasssObj.roll);
console.log(ClasssObj.totalCostForMonth(this.count));
/*2*/
var GreeterMessage = /** @class */ (function () {
    function GreeterMessage(message) {
        this.greeting = message;
    }
    GreeterMessage.prototype.greet = function () {
        return "Hey, " + this.greeting;
    };
    return GreeterMessage;
}());
var greeter = new GreeterMessage("Karim");
console.log(greeter.greet());
