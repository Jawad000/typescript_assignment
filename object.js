"use strict";
/*1*/
var person = {
    firstName: "Tom",
    lastName: "Jerry",
    sayHello: function () { },
    sayBye: function () { }
};
var invokePerson = function (obj) {
    console.log("first name : " + obj.firstName);
    console.log("last name" + obj.lastName);
};
invokePerson(person);
/*2*/
person.sayHello = function () {
    console.log("hello " + person.firstName);
};
person.sayHello();
person.sayBye = function () {
    console.log("BYe " + person.lastName);
};
person.sayBye();
