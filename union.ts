type num = number | string;
function square(n: num): number {
    if (typeof n === 'string') {
        n = parseInt(n);
    }
    return Math.pow(n, 2);
}
console.log(square(3));
console.log(square("5"));

/*2*/

type Alignment = "Left" | "RIGHT" | "CENTER";
function doAlign(alignment: Alignment):void{
    console.log(alignment);
}
doAlign("Left");

type WIDTH = 100 | 200 | 300;
function setWidth(w: WIDTH){
    console.log(w);
}
setWidth(100);

/*3*/

let code: (string | number);
code = 123;   
code = "ABC"; 
code = false; 

let empId: string | number;
empId = 111; 
empId = "E111"; 
empId = true; 