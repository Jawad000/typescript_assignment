
/*1*/

abstract class NewOne
{
    name:string;
    
    constructor(name:string)
    {
        this.name=name;
    }

    display():void
    {
        console.log(this.name);
    }

    abstract find(string): NewOne;
}


class AnotherClass extends NewOne{

    empCode:number;

    constructor( name:string,code:number)
    {
        super(name);
        this.empCode=code;
    }

    find(name:string) :NewOne
    {
        return new AnotherClass(name,1);
    }
}

let empObject:NewOne = new AnotherClass("Jawad",1000);
empObject.display();

let emp2:NewOne = empObject.find('Steve');
console.log(empObject.display());
console.log(emp2);



/*2*/

abstract class BaseEmployee {
        firstName: string;
        lastName: string;
     
        constructor(firstName: string, lastName: string) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
     
        abstract doWork(): void;
    }
     
    class EmployeeOne extends BaseEmployee {
        constructor(firstName: string, lastName: string) {
            super(firstName, lastName);
        }
     
        doWork(): void {
            console.log(`${this.lastName}, ${this.firstName} doing work...`);
        }
    }
     
    let emp1: BaseEmployee = new EmployeeOne('Dana', 'Ryan');
    emp1.doWork(); 




/*3*/
  
    abstract class Shape {
        private _name: string;
    
        constructor(name: string) {
            this._name = name;
        }
    
        public draw(): void {
            console.log("pre drawing " + this._name);
            this.drawShape();
        }

        protected abstract drawShape();
    }
    
    class Square extends Shape {
        private _length: number;
    
        constructor(name: string, length: number) {
            super(name);
            this._length = length;
        }
    
        
        protected drawShape() {
            console.log("drawing square with length "+this._length);
        }
    }
    
    let shape: Shape = new Square("saure", 5 );
    shape.draw();

