"use strict";
var Number1;
(function (Number1) {
    function add(x, y) {
        return x + y;
    }
    Number1.add = add;
})(Number1 || (Number1 = {}));
var String1;
(function (String1) {
    function add(x, y) {
        return x + y;
    }
    String1.add = add;
})(String1 || (String1 = {}));
var p = Number1.add(1, 2);
var q = String1.add("a", "b");
console.log(p);
console.log(q);
