var mytuple = [10,"Hello","World","typeScript"]; 
console.log("Items before push "+mytuple.length)    

mytuple.push(12)                                    
console.log("Items after push "+mytuple.length) 
console.log("Items before pop "+mytuple.length) 
console.log(mytuple.pop()+" popped from the tuple") 
  
console.log("Items after pop "+mytuple.length)



var mytuple = [10,"Hello","World","typeScript"]; 
console.log("Tuple value at index 0 "+mytuple[0]) 


mytuple[0] = 121     
console.log("Tuple value at index 0 changed to   "+mytuple[0])





var a =[10,"hello"] 
var [b,c] = a 
console.log( b )    
console.log( c ) 