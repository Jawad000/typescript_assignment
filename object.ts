/*1*/



var person = {
    firstName : "Tom",
    lastName : "Jerry",
    sayHello:function() {  },
    sayBye: function() {  }
};

var invokePerson = function(obj:{firstName:string,lastName:string})
{
    console.log("first name : " + obj.firstName);
    console.log("last name" + obj.lastName);
}

invokePerson(person);


/*2*/

 person.sayHello = function() {  
    console.log("hello "+person.firstName)
 }  
 person.sayHello()
 
 person.sayBye = function()
 {
     console.log("BYe "+ person.lastName);
 }

 person.sayBye();

 // *3//
 
const obj = {
    x: 10,
    y: 20,
    getSum: function (): number {
      return this.x + this.y;
    }
  };
  
  // call the function getSum()
  console.log("Sum: " + obj.getSum());