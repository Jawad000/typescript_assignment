interface IEmployee {
    empCode: number;
    name: string;
    getSalary:(number)=>number;
}

class Employee implements IEmployee { 
    empCode: number;
    name: string;

    constructor(code: number, name: string) { 
                this.empCode = code;
                this.name = name;
    }

    getSalary(empCode:number):number { 
        return 20000;
    }
}

let empObj = new Employee(1, "Steve");

console.log(empObj);






/*-----*/

interface Library
{
    bookName:string,
    LibrarianName:string,
    bookBorrowerName:string,
    transictionId:number;
}

class borrower implements Library
{
    bookName;
    LibrarianName;
    bookBorrowerName;
    transictionId;


    constructor(bookName:string,
        LibrarianName:string,
        bookBorrowerName:string,
        transictionId:number)
    {
        this.bookName=bookName;
        this.LibrarianName=LibrarianName;
        this.bookBorrowerName=bookBorrowerName;
        this.transictionId=transictionId;
    }

    CostFOrBorrow(transictionId:number):number{
        return transictionId*1000;
    }

    DetailsofTransiction()
    {
        console.log(`Md ${this.bookBorrowerName} has borrowed ${this.bookName} from ${this.LibrarianName} having the cost for ${this.CostFOrBorrow(this.transictionId)} dollar`);

    }
}


let borrower1= new borrower('Gitanjali','GolapMia','Jawad Amir',12342);

borrower1.DetailsofTransiction();




















