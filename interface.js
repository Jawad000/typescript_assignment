"use strict";
var Employee = /** @class */ (function () {
    function Employee(code, name) {
        this.empCode = code;
        this.name = name;
    }
    Employee.prototype.getSalary = function (empCode) {
        return 20000;
    };
    return Employee;
}());
var empObj = new Employee(1, "Steve");
console.log(empObj);
var borrower = /** @class */ (function () {
    function borrower(bookName, LibrarianName, bookBorrowerName, transictionId) {
        this.bookName = bookName;
        this.LibrarianName = LibrarianName;
        this.bookBorrowerName = bookBorrowerName;
        this.transictionId = transictionId;
    }
    borrower.prototype.CostFOrBorrow = function (transictionId) {
        return transictionId * 1000;
    };
    borrower.prototype.DetailsofTransiction = function () {
        console.log("Md " + this.bookBorrowerName + " has borrowed " + this.bookName + " from " + this.LibrarianName + " having the cost for " + this.CostFOrBorrow(this.transictionId) + " dollar");
    };
    return borrower;
}());
var borrower1 = new borrower('Gitanjali', 'GolapMia', 'Jawad Amir', 12342);
borrower1.DetailsofTransiction();
