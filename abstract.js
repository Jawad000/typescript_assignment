"use strict";
/*1*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NewOne = /** @class */ (function () {
    function NewOne(name) {
        this.name = name;
    }
    NewOne.prototype.display = function () {
        console.log(this.name);
    };
    return NewOne;
}());
var AnotherClass = /** @class */ (function (_super) {
    __extends(AnotherClass, _super);
    function AnotherClass(name, code) {
        var _this = _super.call(this, name) || this;
        _this.empCode = code;
        return _this;
    }
    AnotherClass.prototype.find = function (name) {
        return new AnotherClass(name, 1);
    };
    return AnotherClass;
}(NewOne));
var empObject = new AnotherClass("Jawad", 1000);
empObject.display();
var emp2 = empObject.find('Steve');
console.log(empObject.display());
console.log(emp2);
/*2*/
var BaseEmployee = /** @class */ (function () {
    function BaseEmployee(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return BaseEmployee;
}());
var EmployeeOne = /** @class */ (function (_super) {
    __extends(EmployeeOne, _super);
    function EmployeeOne(firstName, lastName) {
        return _super.call(this, firstName, lastName) || this;
    }
    EmployeeOne.prototype.doWork = function () {
        console.log(this.lastName + ", " + this.firstName + " doing work...");
    };
    return EmployeeOne;
}(BaseEmployee));
var emp1 = new EmployeeOne('Dana', 'Ryan');
emp1.doWork();
/*3*/
var Shape = /** @class */ (function () {
    function Shape(name) {
        this._name = name;
    }
    Shape.prototype.draw = function () {
        console.log("pre drawing " + this._name);
        this.drawShape();
    };
    return Shape;
}());
var Square = /** @class */ (function (_super) {
    __extends(Square, _super);
    function Square(name, length) {
        var _this = _super.call(this, name) || this;
        _this._length = length;
        return _this;
    }
    Square.prototype.drawShape = function () {
        console.log("drawing square with length " + this._length);
    };
    return Square;
}(Shape));
var shape = new Square("saure", 5);
shape.draw();
