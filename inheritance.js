"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TechIndurtries = /** @class */ (function () {
    function TechIndurtries(IndustryName, income) {
        this.IndustryName = IndustryName;
        this.income = income;
    }
    TechIndurtries.prototype.earn = function (income) {
        if (income === void 0) { income = 0; }
        console.log(this.IndustryName + " Earn in year " + this.income + " dollar.");
    };
    return TechIndurtries;
}());
var Google = /** @class */ (function (_super) {
    __extends(Google, _super);
    function Google() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Google.prototype.employee = function (employeeNumber) {
        console.log("Google has mostprobably " + employeeNumber);
    };
    return Google;
}(TechIndurtries));
var Microsoft = /** @class */ (function (_super) {
    __extends(Microsoft, _super);
    function Microsoft() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Microsoft.prototype.employee = function (employeeNumber) {
        console.log("Microsioft has mostprobably " + employeeNumber + " employee.");
    };
    return Microsoft;
}(TechIndurtries));
var googleObj = new Google('Google', 1222);
var MicrosioftObj = new Microsoft('Microsoft', 10101001);
console.log(googleObj.earn());
console.log(MicrosioftObj.earn());
console.log(MicrosioftObj.employee(121));
/*2*/
var Animal = /** @class */ (function () {
    function Animal(theName) {
        this.name = theName;
    }
    Animal.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 0; }
        console.log(this.name + " moved " + distanceInMeters + "m.");
    };
    return Animal;
}());
var Snake = /** @class */ (function (_super) {
    __extends(Snake, _super);
    function Snake(name) {
        return _super.call(this, name) || this;
    }
    Snake.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 5; }
        console.log("Slithering...");
        _super.prototype.move.call(this, distanceInMeters);
    };
    return Snake;
}(Animal));
var Horse = /** @class */ (function (_super) {
    __extends(Horse, _super);
    function Horse(name) {
        return _super.call(this, name) || this;
    }
    Horse.prototype.move = function (distanceInMeters) {
        if (distanceInMeters === void 0) { distanceInMeters = 45; }
        console.log("Galloping...");
        _super.prototype.move.call(this, distanceInMeters);
    };
    return Horse;
}(Animal));
var sam = new Snake("Sammy the Python");
var tom = new Horse("Tommy the Palomino");
sam.move();
tom.move(34);
var Person = /** @class */ (function () {
    function Person(name) {
        this.name = name;
    }
    return Person;
}());
var EmployeeNew = /** @class */ (function (_super) {
    __extends(EmployeeNew, _super);
    function EmployeeNew(name, department) {
        var _this = _super.call(this, name) || this;
        _this.department = department;
        return _this;
    }
    EmployeeNew.prototype.getElevatorPitch = function () {
        return "Hello, my name is " + this.name + " and I work in " + this.department + ".";
    };
    return EmployeeNew;
}(Person));
var howard = new EmployeeNew("Howard", "Sales");
console.log(howard.getElevatorPitch());
