"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Circle = /** @class */ (function () {
    function Circle() {
    }
    Circle.calculateArea = function (radius) {
        return this.pi * radius * radius;
    };
    Circle.pi = 3.14;
    return Circle;
}());
console.log(Circle.pi);
console.log(Circle.calculateArea(5));
var MYCircle = /** @class */ (function () {
    function MYCircle() {
        this.pi = 3;
    }
    MYCircle.pi = 3.14;
    return MYCircle;
}());
MYCircle.pi; // returns 3.14
var circleObj = new MYCircle();
circleObj.pi; // returns 3
var THeCircle = /** @class */ (function () {
    function THeCircle() {
    }
    THeCircle.calculateArea = function (radius) {
        return this.pi * radius * radius;
    };
    THeCircle.prototype.calculateCircumference = function (radius) {
        return 2 * Circle.pi * radius;
    };
    THeCircle.pi = 3.14;
    return THeCircle;
}());
Circle.calculateArea(5); // returns 78.5
var circleObjnew = new THeCircle();
circleObjnew.calculateCircumference(5); // returns 31.4000000
//circleObj.calculateArea(); <-- cannot call this
